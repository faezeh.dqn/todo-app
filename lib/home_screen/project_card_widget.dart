
import 'package:flutter/material.dart';
class projectCard extends StatelessWidget {
  projectCard(
      {@required this.colour, @required this.title, @required this.right});
  Color colour;
  String title;
  double right;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 10.0),
      child: Container(
        child: Column(
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(right: right, top: 20.0),
              child: Text(
                '$title',
                style: TextStyle(fontSize: 30.0, color: Colors.white),
              ),
            ),
          ],
        ),
        width: 240.0,
        decoration: BoxDecoration(
          color: colour,
          borderRadius: BorderRadius.all(
            Radius.circular(25.0),
          ),
        ),
      ),
    );
  }
}
