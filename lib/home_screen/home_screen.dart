import 'package:faziotodo/core/service_locator.dart';
import 'package:faziotodo/core/viewModels/home_screen_viewmodel.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';
import 'project_card_widget.dart';
import 'package:intl/intl.dart';
import 'package:flutter/cupertino.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  bool isSearching = false;
  bool closeOverlayEntry = false;
  String titleOfButton = 'Choose Time';
  bool expansionChanged = false;
  showOverlay(BuildContext context) async {
    OverlayState overlaystate = Overlay.of(context);
    OverlayEntry overlayentry = OverlayEntry(
      builder: (context) => Padding(
        padding:
            EdgeInsets.only(left: 10.0, bottom: 450.0, right: 228.0, top: 70.0),
        child: Container(
          decoration: BoxDecoration(
            color: Color(0xFFeae8e3),
            borderRadius: BorderRadius.all(
              Radius.circular(25.0),
            ),
          ),
          child: Column(
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(right: 80.0 , top: 10.0),
                child: FlatButton(

                  child: Text(
                    'Today',
                    style:
                        TextStyle(fontSize: 15.0, fontWeight: FontWeight.w500),
                  ),
                  onPressed: () {
                    setState(() {

                      titleOfButton = 'Today';
                    },);
                  },
                ),
              ),
              FlatButton(
                child: Padding(
                  padding: EdgeInsets.only(right: 50.0, top: 30.0),
                  child: Text(
                    'Tomorrow',
                    style:
                        TextStyle(fontSize: 15.0, fontWeight: FontWeight.w500),
                  ),
                ),
                onPressed: () {
                  setState(() {
                    titleOfButton = 'Tomorrow';
                  },);
                },
              ),
              FlatButton(
                child: Padding(
                  padding: EdgeInsets.only(right: 50.0, top: 60.0),
                  child: Text('Next Week',
                      style: TextStyle(
                          fontSize: 15.0, fontWeight: FontWeight.w500),),
                ),
                onPressed: () {
                  setState(() {
                    titleOfButton = 'Next Week';
                  },);
                },
              ),
            ],
          ),
        ),
      ),
    );
    overlaystate.insert(overlayentry);
    await Future.delayed(Duration(seconds: 2),);
    overlayentry.remove();

  }

  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<HomeScreenViewModel>.reactive(
      viewModelBuilder: () => locator<HomeScreenViewModel>(),
      disposeViewModel: false,
      builder: (context, model, child) => SafeArea(
        child: Scaffold(
          floatingActionButton: Builder(
            builder: (context) => Container(
              width: 70.0,
              height: 70.0,
              child: FloatingActionButton(
                backgroundColor: Color(0xFFff4c00),
                child: Icon(Icons.add),
                onPressed: () {
                  showModalBottomSheet(
                    isScrollControlled: true,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.vertical(
                        top: Radius.circular(25.0),
                      ),
                    ),
                    context: context,
                    builder: (context) {
                      return CustomBottomSheet();
                    },
                  );
                },
              ),
            ),
          ),
          body: Column(
            children: <Widget>[
              isSearching == false
                  ? Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.only(left: 20.0),
                          child: Text(
                            '$titleOfButton',
                            style: TextStyle(
                                fontSize: 17, fontWeight: FontWeight.w500),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(right: 150.0),
                          child: IconButton(
                            onPressed: () {
                              setState(
                                () {
                                  showOverlay(context);
                                },
                              );
                            },
                            icon: Icon(
                              Icons.keyboard_arrow_down,
                              color: Colors.black,
                            ),
                          ),
                        ),
                        IconButton(
                          onPressed: () {
                            setState(() {
                              isSearching = !isSearching;
                            });
                          },
                          icon: Icon(
                            Icons.search,
                            color: Colors.black,
                            size: 28.0,
                          ),
                        ),
                      ],
                    )
                  : Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Expanded(
                          child: TextField(
                            decoration: InputDecoration(
                              hintText: '      Search here',
                            ),
                          ),
                        ),
                        IconButton(
                          onPressed: (){
                            setState(() {
                              isSearching = !isSearching;
                            });
                          },
                          icon: Icon(Icons.close , color: Colors.black,),
                        )
                      ],
                    ),
              Padding(
                padding: EdgeInsets.only(right: 250.0, top: 20.0),
                child: Text(
                  'Categories',
                  style: TextStyle(
                      fontSize: 20.0,
                      color: Colors.black,
                      fontWeight: FontWeight.bold),
                ),
              ),
              Flexible(
                child: FractionallySizedBox(
                  heightFactor: 0.65,
                  child: Padding(
                    padding: EdgeInsets.only(top: 15.0),
                    child: Container(
                      child: Row(
                        children: <Widget>[
                          Flexible(
                            child: ListView(
                              scrollDirection: Axis.horizontal,
                              children: <Widget>[
                                projectCard(
                                  right: 110.0,
                                  title: 'Home',
                                  colour: Color(0xFF101638),
                                ),
                                projectCard(
                                  right: 65.0,
                                  title: 'University',
                                  colour: Color(0xFF516e87),
                                ),
                                projectCard(
                                  right: 90.0,
                                  title: 'Classes',
                                  colour: Colors.amberAccent,
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class CustomBottomSheet extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<HomeScreenViewModel>.reactive(
        viewModelBuilder: () => locator<HomeScreenViewModel>(),
        disposeViewModel: false,
        builder: (context, model, child) => Container(
              child: Column(
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      IconButton(
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        iconSize: 30.0,
                        icon: Padding(
                          padding: EdgeInsets.symmetric(
                              vertical: 1.0, horizontal: 12.0),
                          child: Icon(
                            Icons.close,
                            color: Colors.white,
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(
                            vertical: 25.0, horizontal: 18.0),
                        child: ButtonTheme(
                          minWidth: 70.0,
                          height: 30.0,
                          child: RaisedButton(
                            child: Text(
                              'Add',
                              style: TextStyle(
                                  color: Colors.white, fontSize: 17.0),
                            ),
                            onPressed: () {
                              print('hh');
                            },
                            color: Color(0xFFff4c00),
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(18.0),
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                  ExpansionTile(
                    title: Text(
                      'Choose your category',
                      style: TextStyle(color: Colors.white, fontSize: 18.0),
                    ),
                    children: <Widget>[
                      Wrap(
                        children: model.getCategories(),
                      )
                    ],
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 20.0),
                    child: Row(
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.only(left: 20.0, right: 110.0),
                          child: Text(
                            'Date',
                            style:
                                TextStyle(color: Colors.white, fontSize: 18.0),
                          ),
                        ),
                        Flexible(
                          child: ButtonTheme(
                            minWidth: 230.0,
                            child: FlatButton(
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(18.0),
                              ),
                              child: model.dueDate != null
                                  ? Padding(
                                      padding: EdgeInsets.only(left: 80.0),
                                      child: Text(
                                        DateFormat.yMMMMd()
                                            .format(model.dueDate),
                                        style: TextStyle(color: Colors.white),
                                      ),
                                    )
                                  : Padding(
                                      padding: EdgeInsets.only(left: 110.0),
                                      child: Text(
                                        'Select',
                                        style: TextStyle(
                                            color: Color(0xFFa8b8c4),
                                            fontSize: 18.0),
                                      ),
                                    ),
                              color: Color(0xFF101638),
                              onPressed: () {
                                DatePicker.showDatePicker(
                                  context,
                                  minTime: DateTime.now(),
                                  maxTime: DateTime(2025, 12, 29),
                                  onConfirm: (newDate) {
                                    model.setdueDate(newDate);
                                    print(model.dueDate);
                                  },
                                );
                              },
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 30.0),
                    child: Row(
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.only(left: 20.0, right: 95.0),
                          child: Text(
                            'Time',
                            style:
                                TextStyle(fontSize: 18.0, color: Colors.white),
                          ),
                        ),
                        ButtonTheme(
                          minWidth: 230.0,
                          child: FlatButton(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(18.0),
                            ),
                            child: model.timeOfDay != null
                                ? Padding(
                                    padding: EdgeInsets.only(left: 135.0),
                                    child: Text(
                                      DateFormat.Hm().format(model.timeOfDay),
                                      style: TextStyle(color: Colors.white),
                                    ),
                                  )
                                : Padding(
                                    padding: EdgeInsets.only(left: 130.0),
                                    child: Text(
                                      'Select',
                                      style: TextStyle(
                                          color: Color(0xFFa8b8c4),
                                          fontSize: 18.0),
                                    ),
                                  ),
                            color: Color(0xFF101638),
                            onPressed: () {
                              DatePicker.showTime12hPicker(
                                context,
                                currentTime: DateTime.now(),
                                onConfirm: (newTime) {
                                  model.settimeOfDay(newTime);
                                },
                              );
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 30.0),
                    child: Row(
                      children: <Widget>[
                        Padding(
                            padding: EdgeInsets.only(left: 20.0),
                            child: Text(
                              'Reminder',
                              style: TextStyle(
                                  color: Colors.white, fontSize: 18.0),
                            )),
                        Padding(
                          padding: EdgeInsets.only(left: 210.0),
                          child: CupertinoSwitch(
                            activeColor: Color(0xFFff4c00),
                            value: false,
                            onChanged: (newVal) {
//                      setState(() {
//                        newVal = isSwitched;
//                      });
                            },
                          ),
                        )
                      ],
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 30.0),
                    child: Row(
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.only(left: 20.0),
                          child: Text(
                            'Detail',
                            style:
                                TextStyle(color: Colors.white, fontSize: 18.0),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Column(
                    children: <Widget>[
                      TextField(
                        onSubmitted: (text) {
//                  setState(() {
//                    model.detail = text;
//                  });
                        },
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 15.0,
                            letterSpacing: 0.3),
                        decoration: InputDecoration(
                          border: InputBorder.none,
                          hintText: '     Add notes',
                          hintStyle: TextStyle(
                            color: Color(0xFFa8b8c4),
                          ),
                        ),
                      ),
                    ],
                  )
                ],
              ),
              height: 600.0,
              decoration: BoxDecoration(
                color: Color(0xFF101638),
                borderRadius: BorderRadius.only(
                  topLeft: const Radius.circular(25.0),
                  topRight: const Radius.circular(25.0),
                ),
              ),
            ));
  }
}

class CategoryChip extends StatelessWidget {
  final Color activeColor;
  final Color inActiveColor;
  final bool isSelected;
  final String name;

  CategoryChip(
      {this.activeColor, this.inActiveColor, this.name, this.isSelected});

  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<HomeScreenViewModel>.nonReactive(
        viewModelBuilder: () => locator<HomeScreenViewModel>(),
        disposeViewModel: false,
        builder: (context, model, child) => FlatButton(
              child: Chip(
                backgroundColor: isSelected ? activeColor : inActiveColor,
                label: Text(
                  name,
                  style: TextStyle(fontSize: 15.0, fontWeight: FontWeight.bold),
                ),
              ),
              onPressed: () {
                model.chosenCategory(name);
              },
            ));
  }
}
