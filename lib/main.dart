import 'dart:ffi';
import 'package:faziotodo/core/service_locator.dart';
import 'package:faziotodo/home_screen/home_screen.dart';
import 'package:flutter/material.dart';


void main() {
  setup();
  return runApp(faziootodo());
}

class faziootodo extends StatefulWidget {
  @override
  _faziootodoState createState() => _faziootodoState();
}

class _faziootodoState extends State<faziootodo> {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home:HomeScreen()
    );
  }
}
