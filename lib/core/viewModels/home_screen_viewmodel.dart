import 'package:faziotodo/core/models/category.dart';
import 'package:flutter/material.dart';
import 'package:faziotodo/home_screen/home_screen.dart';
import 'package:stacked/stacked.dart';

class HomeScreenViewModel extends BaseViewModel {
  DateTime _dueDate = DateTime.now();
  bool _isCompleted = false;
  String _name = 'uu';
  DateTime _timeOfDay = DateTime.now();
  String _category = 'uuu';
  String _detail = 'uuuu';


  List<Category> categoryList = [
    Category(
        name: 'Design',
        isSeletced: false,
        activeColor: Colors.yellow[900],
        inActiveColor: Colors.yellow[200]),
    Category(
        name: 'University',
        isSeletced: false,
        activeColor: Colors.deepOrange[900],
        inActiveColor: Colors.deepOrange[200]),
    Category(
        name: 'Homework',
        isSeletced: false,
        activeColor: Colors.green[900],
        inActiveColor: Colors.green[200])
  ];

  List<CategoryChip> getCategories() {
    List<CategoryChip> chips = [];
    for (Category category in categoryList) {
      CategoryChip categoryChip = CategoryChip(
        name: category.name,
        isSelected: category.isSeletced,
        activeColor: category.activeColor,
        inActiveColor: category.inActiveColor,
      );
      chips.add(categoryChip);
    }
    return chips;
  }

  chosenCategory(String name) {
    for (Category category in categoryList) {
      if (category.name == name) {
        category.isSeletced = true;
      } else {
        category.isSeletced = false;
      }
    }
    notifyListeners();
  }

  String get category {
    return _category;
  }

  DateTime get dueDate => _dueDate;
  setdueDate(DateTime dueDate) {
    _dueDate = dueDate;
    notifyListeners();
  }

  bool get isCompleted => _isCompleted;
  set isCompleted(bool isCompleted) {
    _isCompleted = isCompleted;
    notifyListeners();
  }

  String get name => _name;
  set name(String name) {
    _name = name;
    notifyListeners();
  }

  DateTime get timeOfDay => _timeOfDay;
  settimeOfDay(DateTime timeOfDay) {
    _timeOfDay = timeOfDay;
    notifyListeners();
  }

  String get detail => _detail;
  set detail(String detail) {
    _detail = detail;
    notifyListeners();
  }

}
