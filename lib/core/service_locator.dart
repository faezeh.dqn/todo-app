import 'package:faziotodo/core/viewModels/home_screen_viewmodel.dart';
import 'package:get_it/get_it.dart';

final locator = GetIt.instance;

void setup() {
  locator.registerLazySingleton(() => HomeScreenViewModel());
}