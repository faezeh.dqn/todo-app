class Task {
  DateTime dueDate;
  bool isCompleted;
  String name;
  DateTime timeOfDay;
  String category;
  String detail;

  Task(
      {this.detail,
      this.isCompleted,
      this.category,
      this.dueDate,
      this.name,
      this.timeOfDay});
}
